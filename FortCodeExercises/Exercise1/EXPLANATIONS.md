﻿1) Add EXPLANATIONS.md file 
2) Add Tests and review coverage
3) Change isDark to Switch/ Case for better readability
4) Change color to Switch/ Case for better readability
5) Change name to Switch/ Case for better readability
6) rename machineName variable in Name property for clarity of class property
7) clean up getMaxSpeed - remove unused absoluteMax, change to use Switch
8) Change trimColor to Switch/ Case for better readability
9) Remove redundant code in TrimColoe
10) Change description to Switch/ Case for better readability
11) move hasMaxSpeed to seperate method for reusability; make private to keep interface
12) refactor description to StringBuilder for better string handling
13) Add machineType Enum to enforce specific types, but keep INT interface for TYPE property
14) Force all Type to use machineTypeEnum for safer code ** INTERFACE CHANGE **
15) Use IsNullOrEmpty in name for clearer string check
16) TODOs for additional work; Force DEfault for String instead of VARS
17) convert Enum name directly from string
18) getMaxSpeed - Use unitialized INT to force complile DEFAULT
19) Convert static methods
20) swap expected/result in Tests
