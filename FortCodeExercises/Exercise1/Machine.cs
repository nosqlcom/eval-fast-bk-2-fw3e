using System;
using System.Text;

namespace FortCodeExercises.Exercise1
{
    // TODO: Check bounds
    public enum machineTypeEnum
    {
        //notSet = -1,
        bulldozer = 0,
        crane = 1,
        tractor = 2,
        truck = 3,
        car = 4,
        //outOfBounds
    }

    public class Machine
    {
        // TODO: Set defaul values as seperate constants
        public string machineName = "";

        // TODO: Set ALL other properties when type is Set for _SPEED_
        public machineTypeEnum type = machineTypeEnum.bulldozer;

        public string name
        {
            get
            {
                var localName = "";
                // TODO: verify should we allow setting the machine name
                if (string.IsNullOrEmpty(this.machineName)) {
                    if (Enum.IsDefined(typeof(machineTypeEnum), this.type))
                        localName = this.type.ToString();
                }
                return localName;
            }
        }

        public string description
        {
            get
            {
                int maxSpeed = Machine.getMaxSpeed(this.type, this.hasMaxSpeed);

                // TODO:  Clean up concat. The string builder has mutiple steps for code comparison of original
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.Append(" ");
                stringBuilder.Append(this.color + " ");
                stringBuilder.Append(this.name);
                stringBuilder.Append(" ");
                stringBuilder.Append("[");
                stringBuilder.Append(maxSpeed.ToString() + "].");
                string description = stringBuilder.ToString();

                return description;
            }
        }


        private bool hasMaxSpeed
        {
            get
            {
                bool hasMaxSpeedLocal;

                switch (this.type)
                {
                    case machineTypeEnum.crane:
                        hasMaxSpeedLocal = true;
                        break;

                    case machineTypeEnum.tractor:
                        hasMaxSpeedLocal = true;
                        break;

                    case machineTypeEnum.truck:
                        hasMaxSpeedLocal = false;
                        break;

                    case machineTypeEnum.car:
                        hasMaxSpeedLocal = false;
                        break;

                    //case machineTypeEnum.bulldozer:     //Case 0 not set in origial IF/ELSE
                    default:
                        hasMaxSpeedLocal = true;
                        break;
                }

                return hasMaxSpeedLocal;
            }
        }
        
        public string color
        {
            get
            {
                string colorLocal; // TODO: Use String to force complile DEFAULT
                switch (this.type)
                {
                    case machineTypeEnum.bulldozer:
                        colorLocal = "red";
                        break;
                    case machineTypeEnum.crane:
                        colorLocal = "blue";
                        break;
                    case machineTypeEnum.tractor:
                        colorLocal = "green";
                        break;
                    case machineTypeEnum.truck:
                        colorLocal = "yellow";
                        break;
                    case machineTypeEnum.car:
                        colorLocal = "brown";
                        break;
                    default:
                        colorLocal = "white";
                        break;
                }

                return colorLocal;
            }
        }

        public string trimColor
        {
            get
            {
                // This can be refacted to one step, but may lose the original design

                var baseColor = this.color;
                string trimColorLocal; // TODO: Use String to force complile DEFAULT
                switch (this.type)
                {
                    case machineTypeEnum.crane:
                        if (Machine.isDark(baseColor))
                            trimColorLocal = "black";
                        else
                            trimColorLocal = "white";
                        break;

                    case machineTypeEnum.tractor:
                        if (Machine.isDark(baseColor))
                            trimColorLocal = "gold";
                        else
                            trimColorLocal = "";
                        break;

                    case machineTypeEnum.truck:
                        if (Machine.isDark(baseColor))
                            trimColorLocal = "silver";
                        else
                            trimColorLocal = "";
                        break;

                    //case machineTypeEnum.bulldozer: //Case 0 not set in origial IF/ELSE
                    //case machineTypeEnum.car:
                    default:
                        trimColorLocal = "";
                        break;
                }
                
                return trimColorLocal;
            }
        }

        static public bool isDark(string color)
        {
            bool isDark;
            switch (color)
            {
                case "red":
                case "green":
                case "black":
                case "crimson":
                    isDark = true;
                    break;

                case "yellow":
                case "white":
                case "beige":
                case "babyblue":
                    isDark = false;
                    break;

                default:
                    isDark = false;
                    break;
            }

            return isDark;
        }
        
        static public int getMaxSpeed(machineTypeEnum machineType, bool noMax = false) {
            // Use unitialized INT to force complile DEFAULT
            int max;
            
            switch(machineType)
            {
                case machineTypeEnum.bulldozer:
                    if (noMax) 
                        max = 80;
                    else
                        max = 70;
                    break;

                case machineTypeEnum.crane:
                    if (noMax) 
                        max = 75;
                    else
                        max = 70;
                    break;

                case machineTypeEnum.tractor:
                    if (noMax)
                        max = 90;
                    else
                        max = 60;
                    break;

                case machineTypeEnum.car:
                    if (noMax)
                        max = 90;
                    else
                        max = 70;
                    break;

                default:
                    max = 70;
                    break;
            }

            return max;
        }
    }
}