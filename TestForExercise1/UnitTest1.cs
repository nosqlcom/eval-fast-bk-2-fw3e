using FortCodeExercises.Exercise1;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestForExercise1
{
    [TestClass]
    public class UnitTest1
    {
        [DataTestMethod]
        [DataRow(0, false, "bulldozer", " red bulldozer [80].", "red", "", 70)]
        [DataRow(1, false, "crane", " blue crane [75].", "blue", "white", 70)]
        [DataRow(2, false, "tractor", " green tractor [90].", "green", "gold", 60)]
        [DataRow(3, false, "truck", " yellow truck [70].", "yellow", "", 70)]
        [DataRow(4, false, "car", " brown car [70].", "brown", "", 70)]
        [DataRow(-1, false, "", " white  [70].", "white", "", 70)]
        [DataRow(1, false, "", " blue  [75].", "blue", "white", 70, "newMachineName")]

        [DataRow(0, true, "bulldozer", " red bulldozer [80].", "red", "", 80)]
        [DataRow(1, true, "crane", " blue crane [75].", "blue", "white", 75)]
        [DataRow(2, true, "tractor", " green tractor [90].", "green", "gold", 90)]
        [DataRow(3, true, "truck", " yellow truck [70].", "yellow", "", 70)]
        [DataRow(4, true, "car", " brown car [70].", "brown", "", 90)]
        [DataRow(-1, true, "", " white  [70].", "white", "", 70)]
        [DataRow(1, true, "", " blue  [75].", "blue", "white", 75, "newMachineName")]

        public void TestAllTypes(int setType, bool setNoMax, string expectedName, string expectedDescription, string expectedColor, string expectedTrimColor, int expectedMaxSpeed, string setMachineName = null)
        {
            Machine testMachine = new Machine();
            machineTypeEnum machineType = (machineTypeEnum) setType;

            if (!string.IsNullOrEmpty(setMachineName)) testMachine.machineName = setMachineName;
            testMachine.type = machineType;

            Assert.AreEqual(expectedName, testMachine.name);
            Assert.AreEqual(expectedDescription, testMachine.description);
            Assert.AreEqual(expectedColor, testMachine.color);
            Assert.AreEqual(expectedTrimColor, testMachine.trimColor);
            Assert.AreEqual(expectedMaxSpeed, Machine.getMaxSpeed(machineType, setNoMax));
        }

        [DataTestMethod]
        [DataRow("red", true)]
        [DataRow("yellow", false)]
        [DataRow("green", true)]
        [DataRow("black", true)]
        [DataRow("white", false)]
        [DataRow("beige", false)]
        [DataRow("babyblue", false)]
        [DataRow("crimson", true)]
        [DataRow("", false)]
        [DataRow("notAColor", false)]
        [DataRow(null, false)]
        public void TestisDark(string color, bool isDarkExpected)
        {
            //Machine testMachine = new Machine();
            Assert.AreEqual(isDarkExpected, Machine.isDark(color));
        }

    }
}
